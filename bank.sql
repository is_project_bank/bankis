-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2021 at 08:40 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `idClient` int(11) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `idClient`, `currency`, `amount`) VALUES
(1, 1, 'ron', 1856.5),
(2, 2, 'ron', 2001.6),
(3, 3, 'ron', 1954),
(4, 1, 'eur', 380),
(5, 3, 'eur', 400),
(6, 2, 'eur', 390),
(7, 4, 'ron', 100),
(8, 5, 'ron', 100),
(9, 6, 'ron', 100),
(10, 7, 'ron', 100),
(11, 8, 'ron', 100),
(12, 9, 'ron', 100),
(13, 10, 'ron', 100),
(14, 11, 'ron', 100),
(15, 12, 'ron', 100);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `firstname`, `lastname`, `phone`, `email`, `username`, `password`) VALUES
(1, 'Alexandra', 'Giurgiu', '0744444444', 'alexandra@gmail.com', 'alexandra', 'alexandra'),
(2, 'Larisa', 'Dud', '0755555555', 'larisa@gmail.com', 'larisa', 'larisa'),
(3, 'Andra', 'Buzila', '0766666666', 'andra@gmail.com', 'andra', 'andra'),
(4, 'Andrei', 'Burian', '0765832145', 'buri@gmail.com', 'buri', 'buri'),
(5, 'Dragos', 'Contiu', '0756985236', 'dragos@gmail.com', 'dragos', 'dragos'),
(6, 'Emilian', 'Anton', '0711112222', 'emi@gmail.com', 'emi', 'emi'),
(7, 'Alexandru', 'Cindea', '0745896321', 'cindea@gmail.com', 'cindea', 'cindea'),
(8, 'Ovidiu', 'Golban', '0741232147', 'ovi@gmail.com', 'ovi', 'ovi'),
(9, 'Zoltan', 'Darlaczi', '0785258525', 'zoli@gmail.com', 'zoli', 'zoli'),
(10, 'Andrei', 'Cengher', '0784123658', 'cengher@gmail.com', 'cengher', 'cengher'),
(11, 'Gabriel', 'Calugar', '0788552233', 'calugar@gmail.com', 'calugar', 'calugar'),
(12, 'Mihai', 'Gavra', '0741474147', 'mihai@gmail.com', 'mihai', 'mihai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idClient` (`idClient`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
