// Get the mysql service
var mysql = require('mysql');

// Add the credentials to access your database
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'bank'
});

// connect to mysql
connection.connect(function(err) {
    // in case of error
    if(err){
        console.log(err.code);
        console.log(err.fatal);
    }
});

// Perform a query
$query = 'SELECT * from client';

connection.query($query, function(err, rows, fields) {
    if(err){
        console.log("An error ocurred performing the query.");
        return;
    }

    console.log("Query succesfully executed: ", rows);
});

function login(username, password){
    var queryLogIn = 'SELECT * from client where username=' + username;
    connection.query(queryLogIn, function (err, result) {
        if (err) throw err;
        console.log(result);})
        if (result==password) console.log("ok"); 
}

// Close the connection
connection.end(function(){
    // The connection has been closed
});
